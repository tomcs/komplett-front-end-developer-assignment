﻿(function ($) {
    $(function () {
        var inactiveSegment = $(".segment-menu a:not(.active), #SegmentChooser a");
        var action = $("#gaq-changeto-segment").val();
        var label = $("#gaq-login-status").val();

        inactiveSegment.click(function (e) {
            
            if ('dataLayer' in window) {
                dataLayer.push({
                    'segmentAction': action,
                    'segmentLabel': label
                });
                
                dataLayer.push({ 'event': 'segment-event' });
                
            } else {
                _gaq.push(["_trackEvent", "SegmentChooser", action, label]);
            }
        });
    });
})(jQuery);
