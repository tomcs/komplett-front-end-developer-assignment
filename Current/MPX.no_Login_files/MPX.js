/* 
QUICK SCRIPT GUIDE
    
@var sc_onload_ie                       - if set to TRUE, script will use onload event, if false domcontentloaded (IE ONLY)
@var sc_use_click                       - if set to TRUE, script will use "fake click" method to execute RunByteSize(). Must be used with DCStormIQ tag (prevents from onload to work)
@var sc_dest                            - data destination, 0 - Staging, 1 - Live

__SCO.setLocale("en-GB")                - setting locale for date conversion feature, __SCO.siteLocalised or __SCO.localeBrowser can be overwritten if necessery

__SCO.titleText                         - set to true if you want onChane to take "text" not "value" from title selects

__SCO.onC = {                           - simplified onChange feature, specify arrays of objects to be "looked at" by onChange feature
"email"     : [object1, object2],
"surname"   : [surnamefield]
}

__SCO.block = {                         - block certain strings from onchange
"email"     : ["string1","string2"],
"name"      : ["name placeholder"]
}
*/


var __sc, sc_onload_ie = true, sc_use_click = false, sc_dest = 1, sc_runCount = 0;
function populate() {
    __sc = { b: "", bs: "1", c: "17141", s: "", n: "", e: "", t: "", o: "", p: "", i: "", v1: "", v2: "", q1: "", q2: "", q3: "", u: "", d1: "", d2: "", cu1: "", cu2: "", w: "", y: "", uc: "1", cc: "", ct: "30", st: "1800", er: "", ifs: "", sfs: "", ctd: "" };

    __sc.w = __SCO.loc;

    if (__SCO.isString(__SCO.loc, "cart")) {
        __sc.s = "1";
        processStatusOne();
        if (typeof testmonitor === "undefined") {
            testmonitor = new sc_monitor();
            testmonitor.func(__SCO.runByteSize);
            testmonitor.element(function () { return __SCO.eclass("shopping-cart-list", "table")[0]; });
            testmonitor.start();
        }
    }
    else if (__SCO.isString(__SCO.loc, "processcard") || __SCO.isString(__SCO.loc, "action=confirmfinancing")) {
        __sc.s = "3";
        try { __sc.sfs = "ordernum^" + __SCO.text(__SCO.tag("a", __SCO.eclass("alertGood")[0])[0]); } catch (e) { }
    }
    else if (__SCO.isString(__SCO.loc, "orderform")) {

        if (__SCO.eclass("alertGood", "div")[0] != null) {

            if (__SCO.isString(__SCO.text(__SCO.eclass("alertGood", "div")[0]), "Takk for bestillingen! Ditt ordrenummer er")) {

                if (!isNaN(__SCO.text(__SCO.tag("a", __SCO.eclass("alertGood", "div")[0])[0]))) {
                    __sc.s = "3";
                    try { __sc.sfs = "ordernum^" + __SCO.text(__SCO.tag("a", __SCO.eclass("alertGood")[0])[0]); } catch (e) { }
                }

            }

        }

        else if (__SCO.eclass("alertGood", "div")[0] == null) {
            __sc.s = "2";
        }

    }

    /* ONCHANGE */
    /* Do not attach on CRM Page */
    if (!__SCO.isString(__SCO.loc, "admin/crm")) {
        __SCO.onC = {
            "email": [__SCO.id("resetPasswordViewModel_Username"), __SCO.name("pflUsername")[0], __SCO.id("EmailAdr1")],
            "name": [__SCO.name("Ecom_BillTo_Postal_Name_First")[0]],
            "surname": [__SCO.name("Ecom_BillTo_Postal_Name_Last")[0]],
            "telephone": [__SCO.name("Ecom_BillTo_Telecom_Mobile_Number")[0]]
            //,"optout"    : [__SCO.name("PCL1")[0]]
        };
    }
}


function sc_monitor() {
    try {
        var a, b, o, n, t, c = 0, i = !1, m = 300, l = clearTimeout, e = setTimeout, v = 500;
        function h() {
            var x = !!__SCO.getDOM(a());
            if (x) { if (typeof a().length == 'undefined') { i = a().tagName.toLowerCase() == "input" ? !!1 : !1 } };
            typeof o == 'undefined' ? o = (i ? a().value : (x ? a().innerHTML : null)) : "";
            n = (i ? a().value : (x ? a().innerHTML : null));
            o != n ? (b.call(), o = n) : '';
            c++;
            c < m ? (l(t), t = e(h, v)) : l(t);
        }
        this.setmax = function (z) { m = z; }
        this.stop = function () { l(t); }
        this.start = function () { (l(t), c = 0, e(h, v)); }
        this.interval = function (y) { v = y; }
        this.element = function (aa) { a = aa; }
        this.func = function (bb) { b = bb; }
    }
    catch (e) { __SCO.error("206 Timer Error"); }
}


function processStatusOne() {
    var imgs = "", itemName = "", itemQty = "", itemPrice = "", totalPrice = "", itemIds = "", custom1 = "", custom2 = "", itemCount = 0, itemFields = "", sessionFields = "", minusPrice = 0, arrival = "", departure = "", adults = "", children = "";

    __SCO.remV.src = "http://|https://|www.mpx.no|/img/p/50/";
    __SCO.remV.href = "";

    if (__SCO.eclass("list", "div")[0] != "") {
        var table = __SCO.getDOM(__SCO.eclass("shopping-cart-list", "table")[0], "1 basket");
        var rows = __SCO.getDOM(__SCO.tag("tbody", table), "2 item rows");
        //var rows2 = __SCO.getDOM(__SCO.eclass("td_bottom", "tr", table), "2 item rows");

        try {
            for (var i = 0; i < rows.length; i++) {
                var imgsL = "", itemNameL = "", itemQtyL = "", itemPriceL = "", itemIdsL = "", custom1L = "", custom2L = "", itemFieldsL = "", colour = "", size = "", adultsL = 0, childrenL = 0, arrivalL = "", departureL = "";

                imgsL = __SCO.remP(__SCO.getDOM(__SCO.tag("img", rows[i])[0], "3 image"), "src");
                //custom1L = __SCO.remP(__SCO.getDOM(__SCO.tag("a", rows[i])[0], "4 product link"), "href");
                itemIdsL = __SCO.inBetween("sku=", "", __SCO.getDOM(__SCO.tag("a", rows[i])[0], "5 id").getAttribute("href"), "ll");
                itemNameL = __SCO.text(__SCO.getDOM(__SCO.tag("h4", rows[i])[0], "6 name"));
                //var desc = __SCO.text(__SCO.tag("h5", rows[i])[0]);
                //var avail = __SCO.text(__SCO.eclass("availability", "div", rows2[i])[0]);
                //itemFieldsL = 'desc^' + desc + '~avail^' + avail;
                itemQtyL = __SCO.getVT(__SCO.getDOM(__SCO.eclass("adjust-input", "input", rows[i])[0], "7 qty"));
                itemPriceL = __SCO.priceCurr(__SCO.text(__SCO.getDOM(__SCO.eclass("unit-price", "td", rows[i])[0], "8 price")));

                // If you do not want to insert a certain product (blocked category etc.) simply set itemQtyL to 0 and it total value will be automatically recalculated
                minusPrice += itemQtyL == 0 ? itemPriceL * itemQtyL : 0;

                // Insert new row only if following conditions are met
                if (itemNameL != "" && itemQtyL != "" && itemPriceL != "" && itemIdsL != "" && itemQtyL != 0) {
                    imgs += imgsL + "|";
                    itemName += itemNameL + "|";
                    itemQty += itemQtyL + "|";
                    itemPrice += itemPriceL + "|";
                    itemIds += itemIdsL + "|";
                    custom1 += custom1L + "|";
                    custom2 += custom2L + "|";
                    itemFields += itemFieldsL + "|";
                    /* TRAVEL
                    adults += adultsL + "|";
                    departure += departureL + "|";
                    arrival += arrivalL + "|";
                    children += childrenL + "|";
                    */
                    itemCount++;
                }
            }
        }
        catch (err) {
            __SCO.error("101 " + err.description);
        }

        try {
            //Set total price and session fields
            totalPrice = __SCO.priceCurr(__SCO.text(__SCO.getDOM(__SCO.eclass("total-sum", "li")[1], "0 total price")));
            // __sc.y = __SCO.curSym;

            var splitID = itemIds.split("|"), splitQty = itemQty.split("|"), link = "";
            for (var a = 0; a < splitID.length - 1; a++) {
                var z = a; z++;
                link += splitID[a] + ";" + splitQty[a] + ((a == splitID.length - 2) ? '' : '%250D%250A');
            }
            sessionFields = "link^" + link;

        }
        catch (errOR) {
            __SCO.error("201 " + errOR.description);
        }

    }
    // do not allow baskets with no total price
    if (itemCount == 0 || totalPrice == "" || totalPrice == 0 || totalPrice == "0.00") {
        __sc.s = __sc.er == "" ? "" : __sc.s;
    }
    else {
        __sc.u = imgs;
        __sc.i = itemName;
        __sc.q1 = itemQty;
        __sc.v1 = itemPrice;
        __sc.p = itemIds;
        __sc.cu1 = custom1;
        __sc.cu2 = custom2;
        __sc.ifs = itemFields;
        __sc.sfs = sessionFields;
        __sc.v2 = (totalPrice - minusPrice).toFixed(2);
        /* TRAVEL SPECIFIC
        __sc.d1 = arrival;
        __sc.d2 = departure;
        __sc.q2 = adults;
        __sc.q3 = children;
        */
    }
}


/***** BYTESIZE GLOBAL *****/
(function (window) {
    var __SCO = {
        block: {},
        curr: {
            /* To handle same symbols please use manual currency detection for example:
                $ : USD, AUD, CAD, NZD, SGD
                � : GBP, EGP, GIP, FKP, LBP, SDG, SHP, SSP, SYP
                kr: NOK, SEK, DKK
            */
            "\u00a3": "GBP", "\u20ac": "EUR", "�": "EUR", "$": "USD", "A$": "AUD", "CAD$": "CAD", "CHF": "CHF", "Fr.": "CHF", "\u00a5": "JPY", "kr": "NOK", "NZ$": "NZD", "\u0440\u0443\u0431.": "RUB", "py6": "RUB", "pyu0431": "RUB", "SKr": "SEK", "Kc": "CZK"
            /* Uncomment if you need all currencies */
            /*,"AED":"AED","AFN":"AFN","Lek":"ALL","AMD":"AMD","Kz":"AOA","AR$":"ARS","\u0192":"AWG","AZN":"AZN","KM":"BAM","Bds$":"BBD","BDT":"BDT","\u043b\u0432":"BGN","BHD":"BHD","Fr":"BIF","BD$":"BMD","B$":"BND","$b":"BOB","R$":"BRL","B$":"BSD","BTN":"BTN","P":"BWP","p.":"BYR","BZ$":"BZD","FC":"CDF","CLP":"CLP","\u00a5":"RMB","COP":"COP","\u00a2":"CRC","$MN":"CUP","Esc":"CVE","CYP":"CYP","Kc":"CZK","K\u010d": "CZK","Fdj":"DJF","DKK":"DKK","Dkr":"DKK","RD$":"DOP","DZD":"DZD","EEK":"EEK","EGP":"EGP","Nfk":"ERN","ETB":"ETB","FJ$":"FJD","FKP":"FKP","GEL":"GEL","GGP":"GGP","GHS":"GHS","D":"GMD", "Fr":"GNF", "q":"GTQ","HK$":"HKD","L":"HNL","kn":"HRK","G":"HTG","Ft":"HUF","Rp":"IDR","ILS":"ILS","\u20aa":"ILS","IMP":"IMP","Rs":"INR","\u20b9":"INR","IQD":"IQD","IRR":"IRR","\u00cdkr":"ISK", "JEP":"JEP","J$":"JMD","JOD":"JOD","Sh":"KES","KGS":"KGS","KHR":"KHR","Fr":"KMF","\u20a9":"KPW","KRW":"KRW","KWD":"KWD","CI$":"KYD","KZT":"KZT","\u20ad":"LAK","LBP":"LBP","Rp":"LKR","L$":"LRD","L":"LSL","Lt":"LTL","Ls":"LVL","LYD":"LYD","MAD":"MAD","L":"MDL","MGA":"MGA","\u0434\u0435\u043d":"MKD","K":"MMK","\u20ae":"MNT","P":"MOP","UM":"MRO","Rp":"MUR","MVR":"MVR","MK":"MWK","MEX$":"MXN","RM":"MYR","MT":"MZN","N$":"NAD","\u20a6":"NGN","C$":"NIO","Rp":"NPR","OMR":"OMR","B/.":"PAB","S/.":"PEN","K":"PGK","Php":"PHP","\u20b1":"PHP","Rp":"PKR","z\u0142":"PLN","Gs":"PYG","QAR":"QAR","RMB":"RMB","lei":"RON","Fr":"RWF","SAR":"SAR","SI$":"SBD","Rp":"SCR","SDG":"SDG","SEK":"SEK","SG$":"SGD","S$":"SGD","SHP":"SHP","Le":"SLL","S":"SOS","SPL":"SPL","SRD":"SRD","Db":"STD","\u20a1":"SVC","SYP":"SYP","L":"SZL","\u0e3f":"THB","\u0e1a\u0e32\u0e17":"THB","SM":"TJS","m":"TMM","TND":"TND","T$":"TOP","TL":"TRY","TT$":"TTD","TV$":"TVD","$T":"TVD","NT$":"TWD", "Sh":"TZS","UAH":"UAH","Sh":"UGX","$U":"UYU","UZS":"UZS","Bs":"VEF","\u20ab":"VND","Vt":"VUV","T":"WST","EC$":"XCD","YER":"YER","R":"ZAR","Zk":"ZMK","Z$":"ZWD","CUC$":"CUC"*/
        },
        curr2: { "EGP": "1", "KWD": "1", "OMR": "1", "JOD": "1" },
        cS: "",
        cC: "",
        loc: window.location.href.toString().toLowerCase(),
        localeBrowser: false,
        onC: {},
        remV: { src: "", href: "" },
        siteLocalised: false,
        titleText: false,
        addA: function (z, y) {
            y = y || z.length, x = ""; for (var i = 0; i < y; i++) { x += z[i]; } return x;
        },
        clean: function (a) {
            return (a != null) ? a.replace(/^\s*|\s*$/g, '').replace(/\s{2,}/g, " ") : '';
        },
        cur: function () {
            var a = [], b = [];
            for (var key in this.curr) { a.push(this.curr[key]); b.push(key); }
            this.cC = a.join("|"),
            this.cS = b.join("|");
        },
        decode: function (a, b) {
            var b = b || 20, c = 0, d = "";
            try { while (c < b && a != d) { d = a; a = decodeURIComponent(a); c++; } } catch (ee) { }
            return a;
        },
        eclass: function (a, b, c, d) {
            if (a != "") {
                c = c || document,
                b = b || "*",
                d = d || 1,
                f = new Array(),
                e = this.tag(b, c);
                for (var i = 0; i < e.length; i++) {
                    if ((d == 1 && e[i].className == a) || (d == 2 && e[i].className.indexOf(a) != -1) || (d == 3 && (e[i].className.search(new RegExp("(^|\\s)" + a.replace(/\$/g, "\\$") + "(\\s|$)")) != -1))) {
                        f.push(e[i]);
                    }
                }
                return f[0] != 'undefined' ? f : '';
            }
        },
        error: function (a) {
            __sc.er = (__sc.er == "") ? a : __sc.er;
            return null;
        },
        esc: function (a) {
            return a.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&").replace(/\s/g, "\\s");
        },
        getChildrenOnly: function (e, g) {
            var a = [], c = 0, b = 0, d = this.tag(e, (g || document));
            a[0] = d[0];
            for (var f = 1; f < d.length; f++) {
                null != a[c] && (b += this.tag(e, a[c]).length, b++, c++, null != d[b] && (a[c] = d[b]));
            }
            return a;
        },
        getDOM: function (a, b) {
            b = b || "";
            if (a != null) {
                if (typeof (a.length) != "undefined") {
                    return (a.length > 0) ? a : this.error(b);
                }
                else {
                    return a;
                }
            }
            else {
                return (b != "") ? this.error(b) : null;
            }
        },
        getVT: function (a) {
            var c = a.tagName.toLowerCase(), e;
            if (c == "input" || c == "select" || c == "textarea") {
                var d = a.type.toLowerCase(), e;
                if (c == "select") {
                    e = (__SCO.titleText == false) ? a.options[a.selectedIndex].value : a.options[a.selectedIndex].text;
                }
                else if (c == "input") {
                    if (d == "checkbox" || d == "radio") {
                        e = (a.selected || a.checked == true) ? "1" : "0";
                    }
                    else {
                        e = typeof a.value == "undefined" ? "" : a.value;
                    }
                }
            }
            else {
                e = '';
            }
            return this.clean(e);
        },
        id: function (a) {
            return document.getElementById(a);
        },
        inBetween: function (a, b, c, d) {
            var d = d || "ff", e = '', f = 0, g = c.indexOf(a), h = c.lastIndexOf(a),
            i = a.length, j = "substring", k = c.lastIndexOf(b);
            if (g != -1 && k != -1) {
                if (a == b) {
                    f = c.match(new RegExp(this.esc(a), 'g'));
                    if (f.length > 1) {
                        e = (d == "ff") ? c[j](g + i, c.indexOf(b, g + i)) : (d == "fl" ? c[j](g + i, k) : e);
                    }
                }
                else {
                    e = (d == "ff") ? c[j](g + i, c.indexOf(b, g + i)) : ((d == "fl") ? c[j](g + i, k) : ((d == "lf") ? c[j](h + i, c.indexOf(b, h + i)) : ((d == "ll") ? c[j](h + i, k) : e)));
                }
            }
            return __SCO.clean(e);
        },
        isString: function (a, b) {
            return (a.indexOf(b) == -1) ? false : true
        },
        isValid: function (a, b) {
            if (!!__SCO.block[b] == true && new RegExp("^(" + __SCO.block[b].join("|") + ")").test(a) == true) {
                return false;
            }
            else {
                switch (b) {
                    case "email":
                        return __SCO.isString(a, "@") ? true : false;
                        break;
                    case "telephone":
                        var a = a.replace(/[^0-9]/gi, ""), c = a.split(new RegExp(a[0])).length - 1;
                        return (a.length > 5 && c != a.length) ? true : false;
                        break;
                    default:
                        return true;
                        break;
                }
            }
        },
        name: function (a) {
            return document.getElementsByName(a);
        },
        onChange: function (a, b) {
            if (this.getDOM(a) != null) {
                var e = a.disabled || false;
                var v = this.getVT(a);
                if (e == true) { a.disabled = false; }
                this.readOnChange(a, v, b);
                a.onchange = function () {
                    var v = __SCO.getVT(a);
                    __SCO.readOnChange(a, v, b);
                    __SCO.custCookie();
                    if ((((b == "title" || b == "optout") && __sc.e != "") || (b != "title" && b != "optout")) && __SCO.isValid(v, b) == true) {
                        if (b == "title" || b == "name" || b == "surname") {
                            v = v.charAt(0).toUpperCase() + v.slice(1);
                        }
                        __sc.s = __sc.s != '' ? __sc.s : '2';
                        __SCO.sendRequest(__sc);
                    }
                }
                if (e == true) { a.disabled = true; }
            }

        },
        priceCurr: function (a, f) {
            var f = f == false ? false : true, g;
            if (a.replace(/[^\d]/g, "") != "") {
                var c = a.replace(/[^\d\,\.]/g, "").match(/[\d]+/g),
                b1 = a.match(new RegExp("(" + this.cC + ")"), "i"),
                b2 = a.match(new RegExp("(" + this.cS.replace(/\$/g, "\\$") + ")"), "i");
                if (b1 != null) {
                    this.curSym = b1[0];
                }
                else if (b2 != null) {
                    this.curSym = this.curr[b2[0]] || '';
                }
                // Special handling of currencies with 1/1000 subunit
                if (!!this.curr2[this.curSym] == true) {
                    g = 4;
                }
                else {
                    g = 3;
                }
                var d = (c.length == 1) ? c[0] : (c[c.length - 1].length < g) ? this.addA(c, c.length - 1) + "." + c[c.length - 1] : this.addA(c);
                return (d != "") ? d : ((f == true) ? this.error("301 price not found") : "0.00");
            }
            else if (a == "" && f == true) {
                this.error("301 price not found");
            }
            else { return "0.00"; }
        },
        processOnChange: function () {
            var r = false;
            for (var a in this.onC) {
                for (var b in this.onC[a]) {
                    if (this.onC[a].hasOwnProperty(b)) {
                        this.onChange(this.onC[a][b], a);
                    }
                }
            }
            //this.custCookie();
            if (__sc.n.split("|").length - 1 > 0) {
                if (__sc.n.split("|")[0] + __sc.n.split("|")[1] != "") {
                    __sc.s = __sc.s != '' ? __sc.s : '2';
                    this.sendRequest(false);
                    r = true;
                }
            }
            else if (__sc.e + __sc.t != "") {
                __sc.s = __sc.s != '' ? __sc.s : '2';
                this.sendRequest(false);
                r = true;
            }
            return r;
        },
        readOnChange: function (a, b, c) { /* a - element, b - value, c - type */
            if (b != "" && __SCO.isValid(b, c) == true) {
                if (c == "name" || c == "surname" || c == "title") {
                    b = b.charAt(0).toUpperCase() + b.slice(1);
                    var n = (__sc.n != "") ? __sc.n.split("|") : (__sc.n = '||'.split("|"));
                    if (c == "name") { n[0] = b; } else if (c == "surname") { n[1] = b; } else { n[2] = b; }
                    __sc.n = n.join("|");
                }
                else {
                    __sc[c.substring(0, 1)] = (__SCO.optNeg == true && c == "optout") ? ((b - 1) * -1) : b;
                }
            }
        },
        remP: function (a, b) {
            return (a != null) ? a.getAttribute(b).replace(new RegExp("(" + this.remV[b].replace(/\?/g, "\\?").replace(/\&/g, "\\&").replace(/\./g, "\\.").replace(/\-/g, "\\-") + ")+", "g"), '') : ''
        },
        setLocale: function (a) {
            __sc.ctd = (this.siteLocalised == true && this.localeBrowser == true) ? (navigator.language || navigator.userLanguage) : a;
        },
        tag: function (a, b) {
            return arguments.length == 1 ? document.getElementsByTagName(a) : this.getDOM(b) != null ? b.getElementsByTagName(a) : '';
        },
        text: function (a) {
            return (a != null) ? this.clean(a.textContent || a.innerText) : '';
        },
        title: function () {
            return this.text(this.getDOM(this.tag("title")[0])) || this.loc;
        },

        /* ##############################
            CORE METHODS - DO NOT MODIFY 
           ############################## */

        addEvent: function () {
            var first = true;
            if (document.addEventListener) { document.addEventListener("load", __SCO.runByteSize, false) } else if (window.attachEvent) { window.attachEvent("onload", __SCO.runByteSize); }
            function hr() {
                try {
                    if (typeof document.readyState == 'undefined' || sc_runCount > 0) { throw new Error("ReadyState"); }
                    (document.readyState == "complete" || document.readyState == "loaded") && sc_runCount == 0 ? first ? (setTimeout(hr, 500), first = false) : __SCO.runByteSize() : setTimeout(hr, 50);
                }
                catch (e) { }
            } hr();
        },

        chunk: function (a, b) {
            var bValid = true, chr = "&|=|/";
            var sc_n = Math.floor(a.length / b);
            if (a.length % b != 0) sc_n++;
            for (var i = 1; i < sc_n; i++) {
                if (a.charAt(b * i).match(chr) || a.charAt(b * i - 1).match(chr) || a.charAt(b * i - 2).match(chr) || a.charAt(b * i - 3).match(chr) || a.charAt(b * i - 4).match(chr) || a.charAt(b * i + 1).match(chr) || a.charAt(b * i + 2).match(chr) || a.charAt(b * i + 3).match(chr) || a.charAt(b * i + 4).match(chr)) {
                    bValid = false;
                    break;
                }
            }
            return !bValid ? this.chunk(a, (b - 5)) : b;
        },

        custCookie: function () {
            var c_r = this.fc("__sc");
            if (c_r) {
                var c_s = c_r.split(":"),
                c_n = (c_s[0]) ? c_s[0] : '',
                c_e = (c_s[1]) ? c_s[1] : '',
                c_t = (c_s[2]) ? c_s[2] : '';
                __sc.n = (__sc.n == '' && (c_n != __sc.n)) ? c_n : __sc.n;
                __sc.e = (__sc.e == '' && (c_e != __sc.e)) ? c_e : __sc.e;
                __sc.t = (__sc.t == '' && (c_t != __sc.t)) ? c_t : __sc.t;
            }
            if (__sc.n != '' || __sc.e != '' || __sc.t != '') { this.wrC("__sc", __sc.n + ":" + __sc.e + ":" + __sc.t, __sc.ct); }
        },

        escs: function (a) {
            return escape(a.toString().replace(/&/g, '[sc_amp]').replace(/\?/g, '[sc_qm]').replace(/\+/g, '[sc_pl]').replace(/>/g, '[sc_bc]').replace(/</g, '[sc_bo]'));
        },

        ext: function (a) {
            __sc.cc = 0; if (a == true) { __sc.s = 4; } this.sendData();
        },

        fc: function (a) {
            var eq = a + "=", res = "", ca = document.cookie.split(";");
            for (var i = 0; i < ca.length; i++) {
                var c = this.clean(this.decode(ca[i]));
                res = (c.indexOf(eq) != -1) ? c.substr(eq.length, c.length) : res;
            }
            return (res != '') ? res : null;
        },

        guid: function () {
            function r(a) { return a ? Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) : Math.floor(Math.random() * 1000000000000000).toString(16).substr(0, 12); };
            return ((new Date().getTime()).toString(36) + '-' + r(!!1) + '-' + r(!!1) + '-' + r(!!1) + '-' + r(!1)).toString().toUpperCase();
        },

        hash: function (a) {
            var hash = 0, chr;
            if (a.length == 0) return hash;
            for (i = 0; i < a.length; i++) {
                chr = a.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash = hash & hash;
            }
            return hash.toString();
        },

        machineCookie: function () {
            var c_ms = this.fc("__scSMT"), c_mid = '', c_sid = '', c_st = '', c_is = '', fv = !1;
            if (c_ms) {
                var c_s = c_ms.split(":");
                c_mid = (c_s[0]) ? c_s[0] : '';
                c_sid = (c_s[1]) ? c_s[1] : '';
                c_st = (c_s[2]) ? c_s[2] : '';
                c_is = (c_s[3]) ? c_s[3] : '';
            }
            else {
                fv = !!1;
            }

            // First visit, create all new details
            if (fv) {
                c_mid = this.mid(), c_sid = this.guid(), c_is = __sc.s;
            }
                // If the session has expired OR cookie status == 3 or 5 and the current status is not then reset session (completed order therefore restart session)
            else if ((Math.floor((new Date().getTime() - c_st) / 1000) > __sc.st) || (__sc.s != "3" && c_is == "3")) {
                c_sid = this.guid();
            }
            if (__sc.s != "") { this.wrC("__scSMT", c_mid + ":" + c_sid + ":" + new Date().getTime() + ":" + __sc.s, 1095); }
            // Check has the cookie been written (guard against people having cookies turned off and us generating new machine ID every time) before setting the SC machine and session ID's
            // Presuming FireFox 22 just does not let you set the cookie and doesn't raise an error, this will also take care of Firefox 22
            // This will then cause an auto fall back to using IP and userAgent match up
            if (this.fc("__scSMT")) {
                __sc.mid = c_mid; __sc.b = c_sid;
            }
        },

        mid: function () {
            return new Date().getTime().toString() + Math.floor((1 + Math.random()) * 0x1000000).toString().substr(0, 6);
        },

        runByteSize: function () {
            try {
                sc_runCount++;
                __SCO.cur(); // Process currencies
                populate(); // Populate status, basket information  
                if (__SCO.processOnChange() == false && __sc.s != "") { __SCO.sendData(); } // Send data if status set
            }
            catch (err) { }
        },

        sd: function (d) {
            return new Date(new Date().setDate(new Date().getDate() + (!isNaN(d) ? Number(d) : 30))).toUTCString();
        },

        s3Run: function (a) {
            var m = 25, c = 0;
            if (typeof document.readyState === "undefined") { document.readyState = "complete"; }
            function r() {
                ((document.cookie === "") && c < m && document.readyState !== "complete") ? (c++, setTimeout(r, 40)) : __SCO.s3Run(1);
            }
            if ((document.cookie === "") && document.readyState !== "complete" && arguments.length === 0) {
                r();
            }
            else {
                populate();
                if (typeof __sc == "object" && __sc.hasOwnProperty("s") && __sc.s == "3") { this.sendData(); }
            }
        },

        sendData: function () {
                /* Do not send data on CRM admin page */
            
            if (!this.isString(this.loc, "admin/crm")) {
                var ar = { b: "", mid: "", bs: "", c: "", s: "", n: "", e: "", t: "", o: "", p: "", i: "", v1: "", v2: "", q1: "", q2: "", q3: "", u: "", d1: "", d2: "", cu1: "", cu2: "", w: "", y: "", uc: "", cc: "", ct: "", st: "", er: "", ifs: "", sfs: "", ctv: "", ctd: "", ua: "" };
                var c = __sc.c != null && typeof __sc.c != 'undefined' ? this.escs(__sc.c) : '';
                var len = 0;
                if (c != '') {
                    //this.custCookie();
                    this.machineCookie();
                    // Set the user agent, but in hash form
                    __sc.ua = this.hash(navigator.userAgent);
                    for (a in __sc) {
                        if (__sc[a] != null && typeof __sc[a] != "undefined") {
                            if (a == "i" || a == "p" || a == "n" || a == "ifs" || a == "sfs") {
                                ar[a] = this.escs(encodeURI(__sc[a]));
                            }
                            else {
                                ar[a] = this.escs(__sc[a]);
                            }
                        }
                        else {
                            ar[a] = "";
                        }
                    }

                    var scs = screen.availHeight + '-' + screen.availWidth + '-' + screen.colorDepth + '-' + screen.height + '-' + screen.width,

                    sc_u = ((document.location.protocol == 'https:') ? 'https://' : 'http://') + (sc_dest === 0 ? 'd30ke5tqu2tkyx' : 'd16fk4ms6rqz1v') + '.cloudfront.net/import/' + ((__sc.s == 3 || __sc.s == 5) ? 'pixel' : '') + 'capture.aspx',

                    sc_q = unescape('c=' + ar.c + '&cc=' + ar.cc + '&ca=0&sfs=' + ar.sfs + '&scs=' + scs);
                    if (ar.s == 3) { sc_q += '&b=' + ar.b + '&ua=' + ar.ua; }
                    else if (ar.s == 5) { sc_q += '&e=' + ar.e; }
                    else { sc_q += unescape('&fc=0&mid=' + ar.mid + '&b=' + ar.b + '&n=' + ar.n + '&e=' + ar.e + '&t=' + ar.t + '&o=' + ar.o + '&p=' + ar.p + '&i=' + ar.i + '&u=' + ar.u + '&v1=' + ar.v1 + '&v2=' + ar.v2 + '&q1=' + ar.q1 + '&q2=' + ar.q2 + '&q3=' + ar.q3 + '&d1=' + ar.d1 + '&d2=' + ar.d2 + '&s=' + ar.s + '&w=' + ar.w + '&cu1=' + ar.cu1 + '&cu2=' + ar.cu2 + '&y=' + ar.y + '&bs=' + ar.bs + '&er=' + ar.er + '&ca=0&st=' + ar.st + '&ifs=' + ar.ifs + '&ctd=' + ar.ctd + '&ua=' + ar.ua); }

                    var sc_i = new Image();
                    sc_i.style.display = 'none';
                    if (ar.s == 3 || ar.s == 5 || (navigator.appName != 'Microsoft Internet Explorer' && sc_q.length < 1900) || sc_q.length < 3500) {
                        sc_i.src = sc_u + "?" + sc_q;
                    }
                    else {
                        //Random number - this is used for a new cookie's machine id and for qs chunk processing
                        var len = (sc_q.length > 3500 && navigator.appName != 'Microsoft Internet Explorer') ? 3500 : 1900;
                        var sc_rnd = this.mid();
                        var sc_ch = this.chunk(sc_q, len);
                        if (sc_ch > 0) {
                            var sc_n = Math.floor(sc_q.length / sc_ch);
                            if (sc_q.length % sc_ch != 0) sc_n++;
                            for (var i = 0; i < sc_n; i++) {
                                sc_i.src = sc_u + "?sc_dt=" + sc_rnd + "&sc_pn=" + (i + 1) + "_" + sc_n + "&" + sc_q.substr(i * sc_ch, sc_ch);
                            }
                        }
                    }
                }
            }
        },

        sendRequest: function (a) {
            (a == true) ? this.runByteSize() : this.sendData();
        },

        wrC: function (n, v, d) {
            document.cookie = n + "=" + encodeURIComponent(v) + ";expires=" + this.sd(d) + ";path=/";
        }
    }
    window.__SCO = __SCO;

})(window);
(function () {
    try {
        if (__SCO.isString(__SCO.loc, "processcard") || __SCO.isString(__SCO.loc, "action=confirmfinancing")) {
            __SCO.s3Run();
        }
    }
    catch (e) { }
}());
if (sc_use_click == true) {
    var a = __SCO.tag("body")[0];
    if (a) {
        var b = document.createElement("input");
        b.setAttribute("type", "hidden");
        b.onclick = function () {
            __SCO.runByteSize();
            return false;
        }
        a.appendChild(b);
        b.click();
    }
}
else {
    __SCO.addEvent(sc_onload_ie);
}