define(["Common/events"], function (events) {

    var subscriber = null;
    var messages = [];

    //popupObject = {
    //    showPopup: true,
    //    url: "url as string", //OR
    //    content: "content as string",
    //    popupViewModel: "viewmodel as string",
    //    template: "template id as string",
    //    name: "name on popup for css styling as string"
    //};
    function show(popupObject) {
        if (subscriber) {
            popupObject.showPopup = true;
            subscriber(popupObject);
        } else {
            messages.push(arguments);
        }
    };

    function hide() {
        subscriber({showPopup: false});
    };

    function subscribeTo(callback) {
        subscriber = callback;
        if (messages.length != 0) {
            messages.forEach(function(message) {
                subscriber.apply(null, message);
            });
            messages.length = 0;
        }
        events.PopupToggled.subscribeTo(subscriber);
    }

    return {
        show: show,
        hide: hide,
        subscribeTo: subscribeTo
    };
});
