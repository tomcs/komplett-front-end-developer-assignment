define(["Shared/Popup/popupService"], function (popupService) {

    function PopupViewModel() {

        var self = this;

        this.name = ko.observable();
        this.showPopup = ko.observable(false);
        this.content = ko.observable("");
        this.loading = ko.observable(false);
        this.throttledLoading = ko.observable(false);

        this.popupViewModel = null;

        this.closePopup = function() {
            self.showPopup(false);
            self.content("");
            self.name("");
          
            if (self.customClosePopupHandler)
                self.customClosePopupHandler.apply();
        };

        this.customClosePopupHandler = null;

        this.fillContent = function (data) {
            self.content(data);
            self.loading(false);
            if (self.popupViewModel && self.popupViewModel.afterPopupLoadHandler)
                self.popupViewModel.afterPopupLoadHandler.apply();
        };

        this.popupToggled = function (popupObject) {
            self.loading(true);
            if (popupObject.showPopup) {
                if (popupObject.popupViewModel != null) {
                    self.popupViewModel = popupObject.popupViewModel;
                }
                if (popupObject.url != null) {
                    $.get(popupObject.url, null, self.fillContent);
                }
                else if (popupObject.content != null) {
                    self.fillContent(popupObject.content);
                }
                else if (popupObject.template != null) {
                    self.fillContent($('#' + popupObject.template).html());
                }
                self.name(popupObject.name);
                self.showPopup(true);

            } else {
                self.closePopup();
            }
        };

        init: {
            popupService.subscribeTo(self.popupToggled);
        }
        
    }

    return PopupViewModel;
});
